<?php

function template() {

    return BM::singleton()->getObject('temp');
}

function page() {

    return BM::singleton()->getObject('temp')->getPage();
}

function data_model() {

    return BM::singleton()->getObject('db');
}

function set_type($data) {

    $data = (is_string($data)) ? data_model()->sanitizeData($data) : $data;
    $val = (is_string($data)) ? "'$data'" : $data;
    return $val;
}


function pagina_simple($rutaHtml, $titulo = '', $menu = '') {
    template()->buildFromTemplates('template.html');
    page()->setTitle($titulo);
    page()->addEstigma("menu", $menu);
    template()->addTemplateBit('content', $rutaHtml);
    template()->addTemplateBit('footer', 'footer.html');
    template()->parseOutput();
    template()->parseExtras();
    print page()->getContent();
}

?>