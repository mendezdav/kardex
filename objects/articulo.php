<?php

	class articuloModel extends object{

		/* insercion de nuevo articulo a registro de kardex */
		public function nuevo_articulo($linea, $estilo, $color, $talla){
			
			if(!$this->existe_articulo($linea, $estilo, $color, $talla)){

				$query = "INSERT INTO articulo(linea, estilo, color, talla) VALUES($linea, '{$estilo}', $color, $talla)";

				data_model()->executeQuery($query);

				return true;
			}

			return false;
		}


		/* verificacion de la existencia de un articulo en el registro de kardex */
		public function existe_articulo($linea, $estilo, $color, $talla){
			
			$query = "SELECT COUNT(*) as rows FROM articulo WHERE linea = $linea AND estilo = '{$estilo}' AND color = $color AND talla = $talla";
			
			data_model()->executeQuery($query);

			$resultSet = data_model()->getResult()->fetch_assoc();

			if($resultSet['rows'] > 0){
				
				return true;
			}

			return false;
		}

		public function no_articulo($linea, $estilo, $color, $talla){

			$query = "SELECT id, COUNT(*) as rows FROM articulo WHERE linea = $linea AND estilo = '{$estilo}' AND color = $color AND talla = $talla";
			
			data_model()->executeQuery($query);

			$resultSet = data_model()->getResult()->fetch_assoc();

			if($resultSet['rows'] > 0){
				
				return $resultSet['id'];
			}

			return -1;

		}

	}

?>