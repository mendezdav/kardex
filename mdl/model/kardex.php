<?php

	class kardexModel extends object{
		
		public function nueva_entrada(
			$tran,
			$fecha, 
			$concepto, 
			$dato_articulo, 
			$referencia_recateo_no, 
			$existencias_maximas, 
			$existencias_minimas, 
			$dato_proveedor,
			$periodo,
			$no_ccf, 
			$dato_entrada,
			$numero,
			$bodega){

			$articulo    	  		= $dato_articulo['articulo'];
			$codigo      	  		= $dato_articulo['codigo'];
			$descripcion 	  		= $dato_articulo['descripcion'];
			$nombre_proveedor 		= $dato_proveedor['nombre_proveedor'];
			$nacionalidad_proveedor = $dato_proveedor['nacionalidad_proveedor'];

			$ent_cantidad       = $dato_entrada['ent_cantidad'];
			$ent_costo_unitario = $dato_entrada['ent_costo_unitario'];
			$ent_costo_total 	= $dato_entrada['ent_costo_total'];


			list( $c_actual, $vu_actual, $vt_actual) = $this->estado_actual($codigo, $bodega);

	        $exi_cantidad      	= $c_actual + $ent_cantidad;  		// total (existente + entrante)
	        $exi_costo_total    = $vt_actual + $ent_costo_total;    // total monetario (importe + monto actual)
	        $exi_costo_unitario = $exi_costo_total / $exi_cantidad; // nuevo valor unitario por costo promedio (total monto / total unidades)
	        
			$this->get(0);
			$this->fecha                  = $fecha;
			$this->concepto               = $concepto;
			$this->articulo               = $articulo;
			$this->codigo                 = $codigo;
			$this->descripcion            = $descripcion;
			$this->referencia_recateo_no  = $referencia_recateo_no;
			$this->existencias_maximas    = $existencias_maximas;
			$this->existencias_minimas    = $existencias_minimas;
			$this->nombre_proveedor       = $nombre_proveedor;
			$this->nacionalidad_proveedor = $nacionalidad_proveedor;
			$this->periodo                = $periodo;
			$this->no_ccf                 = $no_ccf;
			$this->ent_cantidad           = $ent_cantidad;
			$this->ent_costo_unitario     = $ent_costo_unitario;
			$this->ent_costo_total        = $ent_costo_total;
			$this->sal_cantidad           = 0;
			$this->sal_costo_unitario     = 0;
			$this->sal_costo_total        = 0;
			$this->exi_cantidad           = $exi_cantidad;
			$this->exi_costo_unitario     = $exi_costo_unitario;
			$this->exi_costo_total        = $exi_costo_total;
			$this->numero                 = $numero; 
			$this->tipo                   = 1;
			$this->bodega                 = $bodega;
			$this->transaccion            = $tran; 


			$this->save();


		}

		public function nueva_salida(
			$tran,
			$fecha, 
			$concepto, 
			$dato_articulo, 
			$referencia_recateo_no, 
			$existencias_maximas, 
			$existencias_minimas, 
			$dato_proveedor,
			$periodo,
			$no_ccf, 
			$dato_salida,
			$numero,
			$bodega){

			$articulo    	  		= $dato_articulo['articulo'];
			$codigo      	  		= $dato_articulo['codigo'];
			$descripcion 	  		= $dato_articulo['descripcion'];
			$nombre_proveedor 		= $dato_proveedor['nombre_proveedor'];
			$nacionalidad_proveedor = $dato_proveedor['nacionalidad_proveedor'];

			$sal_cantidad           = $dato_salida['sal_cantidad'];


			list( $c_actual, $vu_actual, $vt_actual) = $this->estado_actual($codigo, $bodega);

	        $exi_cantidad      	= $c_actual  - $sal_cantidad;  		// total (existente - saliente)
	        $exi_costo_unitario = $vu_actual; // costo unitario no cambia
	        $exi_costo_total    = $vt_actual - ($exi_costo_unitario * $sal_cantidad);    // total monetario (costo actual - costo de salida)
	        
			$this->get(0);
			$this->fecha                  = $fecha;
			$this->concepto               = $concepto;
			$this->articulo               = $articulo;
			$this->codigo                 = $codigo;
			$this->descripcion            = $descripcion;
			$this->referencia_recateo_no  = $referencia_recateo_no;
			$this->existencias_maximas    = $existencias_maximas;
			$this->existencias_minimas    = $existencias_minimas;
			$this->nombre_proveedor       = $nombre_proveedor;
			$this->nacionalidad_proveedor = $nacionalidad_proveedor;
			$this->periodo                = $periodo;
			$this->no_ccf                 = $no_ccf;
			$this->ent_cantidad           = 0;
			$this->ent_costo_unitario     = 0;
			$this->ent_costo_total        = 0;
			$this->sal_cantidad           = $sal_cantidad;
			$this->sal_costo_unitario     = $exi_costo_unitario;
			$this->sal_costo_total        = ($exi_costo_unitario * $sal_cantidad);
			$this->exi_cantidad           = $exi_cantidad;
			$this->exi_costo_unitario     = $exi_costo_unitario;
			$this->exi_costo_total        = $exi_costo_total;
			$this->numero                 = $numero; 
			$this->tipo                   = 2;
			$this->bodega                 = $bodega; 
			$this->transaccion            = $tran; 

			$this->save();


		}

		public function estado_actual($articulo, $bodega){

			$query = "SELECT exi_cantidad, exi_costo_unitario, exi_costo_total FROM kardex WHERE codigo = $articulo AND bodega = $bodega ORDER BY no DESC LIMIT 1";

			data_model()->executeQuery($query);

			if(data_model()->getNumRows() > 0){

				$resulSet = data_model()->getResult()->fetch_assoc();

				return array($resulSet['exi_cantidad'], $resulSet['exi_costo_unitario'], $resulSet['exi_costo_total']);

			}else{

				return array(0,0,0);
			}
		}

		public function kardexReportData($fields = array("*"), $fe_limitInf = "1850-01-01", $fe_limitSup = "2080-01-01", $condiciones = null, $group_by = null, $order_by = null, $order_type = "", $limitInf = null, $tamPag = null, $op_type = "cache" ){
			
			$condicion_str = "";
			$group_by_str =  "";
        	$order_by_str =  "";
        	$limit_str =  "";

        	if(count($fields)>0){

            	$fields = implode(",", $fields);
        	}  

			if(count($condiciones)>0){

	            $condicion_str = "AND ";
	            $condicion_str .= $this->buildCondition($condiciones); 
        	}

        	if(count($group_by)>0){
	            $group_by_str = "GROUP BY ";
	            $group_by_str .= implode(",", $group_by);
	        }

	        if($order_by != null){
	            $order_by_str = "ORDER BY ".$order_by;
	        }

	        if($limitInf!=null){
	            $limit_str = "LIMIT $limitInf";
	        }

	        if($tamPag!=null){
	            $limit_str .= ", $tamPag";
	        }

			$query = "SELECT $fields FROM kardex JOIN articulo ON kardex.codigo = articulo.id WHERE (fecha >= '$fe_limitInf' AND fecha <= '$fe_limitSup') $condicion_str $group_by_str $order_by_str $order_type $limit_str";

			if($op_type=="cache"){

	            return array(data_model()->getNumRows(), data_model()->cacheQuery($query));
	        
	        }else{

	            data_model()->executeQuery($query);
	            $res = array();
	            while($row = data_model()->getResult()->fetch_assoc()){
	                $res[] = $row;
	            }

	            return array(data_model()->getNumRows(), $res);
	        }
		}

		private function buildCondition($condiciones){
	        if(!is_array($condiciones)){
	            return $condiciones;
	        }
	        else if((count($condiciones) == 3)){
	            $condiciones[0] = $this->buildCondition($condiciones[0]);
	            $condiciones[1] = $this->buildCondition($condiciones[1]);
	            $condiciones[2] = $this->buildCondition($condiciones[2]);

	            return implode(" ", $condiciones);
	        }
    	}

	}

?>