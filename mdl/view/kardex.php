<?php

	class kardexView {

		public function product_test($productos){
			template()->buildFromTemplates('test/product_test.html');
			page()->setTitle('Product Test');
			page()->addEstigma('productos', array('SQL', $productos));
			template()->parseExtras();
        	template()->parseOutput();
        	print page()->getContent();
		}

		public function kardex_test(){
			template()->buildFromTemplates('test/kardex_test.html');
			page()->setTitle('Kardex Test');
			template()->parseExtras();
        	@template()->parseOutput();
        	print page()->getContent();
		}

		public function kardexHtmlReport($kardexResult){
			template()->buildFromTemplates('test/kardex_report_test.html');
			page()->setTitle('Kardex report Test');
			page()->addEstigma('kardex_result', array('SQL', $kardexResult));
			template()->parseExtras();
        	template()->parseOutput();
        	return page()->getContent();
		}

	}

?>