<?php

	/**
	*  Gestion de kardex
	*/

	import('mdl.model.kardex');
	import('mdl.view.kardex');

	class kardexController extends controller
	{

		public function product_test(){

			$articulo  = $this->model->get_child('articulo');

			$productos = $articulo->get_list(); 

			$this->view->product_test($productos);
		}

		public function kardex_test(){
			
			$this->view->kardex_test();
		}


		/*** Start Services ***/
		
		public function serviceNuevoProducto(){
			
			if( isset($_POST) && !empty($_POST) ){
				
				$fields   = array('estilo', 'linea', 'color', 'talla');

				foreach ($fields as $field) {

					$$field = ( isset($_POST[$field]) && !empty($_POST[$field]) ) ? $_POST[$field] : null;
				}

				$articulo = $this->model->get_child('articulo');

				$result   = $articulo->nuevo_articulo($linea, $estilo, $color, $talla);

				echo json_encode( array("status"=>200, "result"=>$result) );

			}else{

				echo json_encode( array("status"=>403) );
			}
		}


		public function serviceKardexReport(){

			echo var_dump($this->model->kardexReportData(array("*"), "2015-02-02", "2015-02-02", null, null, null, "", null, null, "data"));

		}

		/*** End Services  ***/

		public function kardexHtmlReport(){
			
			$ent_cantidad 		= "ent_cantidad";
			$ent_costo_unitario = "ent_costo_unitario";
			$ent_costo_total 	= "ent_costo_total";
			$sal_cantidad 		= "sal_cantidad";
			$sal_costo_unitario = "sal_costo_unitario";
			$sal_costo_total 	= "sal_costo_total";
			$exi_cantidad 		= "exi_cantidad";
			$exi_costo_unitario = "exi_costo_unitario";
			$exi_costo_total 	= "exi_costo_total";
			$order_type			= (isset($_POST['order_type'])) ? $_POST['order_type']: "ASC"; 

			$group_by = null;

			$condiciones = array();
			$tipos = "";

			$term = $_POST['proveedor'];

			if( !empty($_POST['linea']) ) 	$condiciones["linea"]  = array("linea","=",$_POST['linea']);
			if( !empty($_POST['estilo']) ) { $estilo = $_POST['estilo']; $condiciones["estilo"] =  array("estilo","=","'{$estilo}'"); }
			if( !empty($_POST['color']) ) 	$condiciones["color"]  = array("color","=",$_POST['color']);
			if( !empty($_POST['talla']) ) 	$condiciones["talla"]  = array("talla","=",$_POST['talla']);
			if( !empty($_POST['proveedor']) ) 	$condiciones["nombre_proveedor"]  = array("nombre_proveedor"," LIKE ","'%{$term}'");
			if( !empty($_POST['proveedor']) ) 	$condiciones["nombre_proveedor"]  = array("nombre_proveedor"," LIKE ","'{$term}%'");
			if( !empty($_POST['proveedor']) ) 	$condiciones["nombre_proveedor"]  = array("nombre_proveedor"," LIKE ","'%{$term}%'");
			
			if(($_POST['entradas'] == 1) && ($_POST['salidas']  == 1)) {
				$tipos = " (tipo = 1 OR tipo = 2) ";
			}else{
				if($_POST['entradas'] == 1)  $condiciones["entradas"]  = array("tipo","=",1);
				if($_POST['salidas']  == 1)  $condiciones["salidas"]  = array("tipo","=",2);
			}

			foreach ($condiciones as $condicion => $valor) {
					
				$condiciones[$condicion] = implode("", $condiciones[$condicion]);
			}

			if(count($condiciones)>0){
				$condiciones = implode(" AND ", $condiciones);
				if(!empty($tipos))
					$condiciones .= " AND ".$tipos;
			}else{
				$condiciones = $tipos;
			}

			$fields = array(
				"no",
				"tipo",
				"fecha",
				"concepto",
				"numero",
				"articulo",
				"linea",
				"bodega",
				"estilo",
				"color",
				"nombre_proveedor",
				"talla",
				"descripcion",
				"$ent_cantidad", 
				"$ent_costo_unitario",
				"$ent_costo_total", 
				"$sal_cantidad", 
				"$sal_costo_unitario",
				"$sal_costo_total", 
				"$exi_cantidad", 
				"$exi_costo_unitario",
				"$exi_costo_total",
			);
 


 			list($rows, $kardexResult) = $this->model->kardexReportData($fields, $_POST['fe_limitInf'], $_POST['fe_limitSup'], $condiciones, $group_by, "no", $order_type, null, null, "cache");
		
 			echo json_encode(array('html' => $this->view->kardexHtmlReport($kardexResult) ));
		}
	}

?>