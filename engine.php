<?php

session_start();

error_reporting(E_ALL);

define("BUSINESS_MANAGER", false); # aplicacion en produccion

require_once('settings/settings.php');  # carga configuraciones
require_once('scripts/encryption.php'); # funciones de proteccion de datos

import('core.bm_engine');

date_default_timezone_set(TIMEZONE);

import('core.engine.database');
import('core.engine.template');
import('core.engine.page');
import('core.engine.frontController');
import('core.handler.http');
import('core.handler.controller');
import('core.handler.object');
import('core.handler.sessionHandler');
import('core.handler.MysqliHandler');
import('core.handler.debugHandler');
import('core.orm.helper');

import('scripts.alias');
import('scripts.init_setup');


import('mdl.error');

#####	configuraciones iniciales	#####

BM::singleton()->storeObject('template', 'temp');
BM::singleton()->storeObject('database', 'db');
BM::singleton()->storeSetting('default', 'skin');

BM::singleton()->getObject('temp')->getPage()->setJs('static/js/jquery.min.js');
BM::singleton()->getObject('temp')->getPage()->setJs('static/js/business.manager.1.0.js');


BM::singleton()->getObject('db')->newConnection(HOST, USER, PASSWORD, DATABASE);

#####	fin de configuraciones #####

$front = new frontController(array());  # crear controlador 'front'
$front->run();        # correr controlador 'front'
exit();
?>